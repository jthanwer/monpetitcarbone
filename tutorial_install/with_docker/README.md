# Installation avec nginx + gunicorn

## 1. Clonage du dépôt GIT

Nous appelons ici `workdir` le chemin-racine (i.e.le dossier) où l'application sera placée sur le serveur.

Placez-vous dans le dossier `workdir` et clonez le dépôt git.

```
~$ cd workdir
~$ git clone https://framagit.org/jthanwer/monpetitcarbone.git
```

## 2. Variables d'environnement

Plusieurs variables d'environnement sont nécessaires au bon fonctionnement de l'application Django. Elles sont détaillées dans le fichier `./variables.env`. Vous devez modifier ce fichier avant de lancer la construction de l'image docker.


## 3. Mise en place de la base de données

Par défaut, l'application utilise une image Docker de PostgreSql. Vous pouvez aussi décider d'utiliser une autre base de données, il suffit pour cela de changer l'image dans le fichier `docker-compose.yml` et d'adapter les variables d'environnement et le port du conteneur. 

## 4. Construction de l'image Docker

Pour construire l'image Docker, il suffit de lancer la commande :

```
~$ cd workdir/monpetitcarbone
~$ docker-compose up -d
```

Après la construction de l'image, l'application devrait être accessible via le port 8080.


## 5. Compte administrateur

Django propose une interface d'administration accessible via `/api/admin`.
Pour créer un compte administrateur Django permettant d'accéder à cette interface, il suffit de lancer la commande suivante :

```
~$ docker-compose exec mpc-backend python manage.py createsuperuser
```

Après avoir renseigné les informations de ce compte, vous pouvez vous connecter à la page administrateur.
