# Installation avec nginx + gunicorn

## 1. Clonage du dépôt GIT

Nous appelons ici `workdir` le chemin-racine (i.e.le dossier) où l'application sera placée sur le serveur.

Placez-vous dans le dossier `workdir` et clonez le dépôt git.

```
~$ cd workdir
~$ git clone https://framagit.org/jthanwer/monpetitcarbone.git
```

## 2. Partie Vuejs

### 2.1 Installation nodejs

Pour installer les dépendances de l'application VueJS et la compiler, il est nécessaire d'installer NodeJS sur le serveur.
Installez la version Node v16.16.0. Certaines dépendances créent des problèmes avec des versions plus récentes.

Un tutoriel est présenté ici :
https://github.com/nodesource/distributions/blob/master/README.md#debinstall

### 2.2 Installation des dépendances

Une fois que nodejs est installé sur le serveur, allez dans le dossier `frontend` de l'application :

```
~$ cd workdir/monpetitcarbone/frontend/

# Installer les dépendances
~$ npm install

# Compilation du code
~$ npm run generate
```

Après la compilation du code, un dossier `./frontend/dist/` est normalement généré. Ce dossier contient le fichier `index.html` et les scripts css et javascript qui permettent ensemble d'afficher correctement l'application sur un navigateur et vers laquelle nginx sera chargé de rediriger les connexions au serveur.

### 2.3 Installation et configuration de nginx

Premièrement, si nginx n'est pas déjà installé sur le serveur, utilisez ces commandes pour le faire.

```
~$ sudo apt-get update
~$ sudo apt-get install nginx -y
```

Pour créer une nouvelle configuration nginx, il est nécessaire de créer un fichier avec la commande ci-dessous :

```
sudo vi /etc/nginx/sites-available/default
```

Vous trouverez un exemple de fichier de configuration ici : `./installation/without_docker/nginx.basic.conf`

Après avoir effectué les modifications, testez celle-ci avec la commande suivante :

```
~$ sudo nginx -t
```

Si aucun message d'erreur n'est renvoyé, vous pouvez continuer. Sinon, vérifiez votre fichier de configuration. La prochaine commande permet de relancer nginx afin de prendre en compte la nouvelle configuration.

```
~$ sudo systemctl restart nginx
```

Normalement, le serveur devrait maintenant renvoyer la page d'accueil de l'application en se rendant à l'addresse indiquée dans la configuration. Cependant, l'application n'est pas encore totalement utilisable car l'API Django/Python n'est pas encore fonctionnelle.

## 3. Partie Django/Python

### 3.1 Installation des dépendances

L'application nécessite l'installation de nombreuses dépendances/modules python. Nous recommandons de les installer dans un environnement virtuel à l'aide de virtualenv. Dans le dossier ./workdir/monpetitcarbone/backend/`, tapez les commandes suivantes :

```
~$ sudo apt-get install virtualenv
~$ cd workdir/monpetitcarbone/backend/
~$ virtualenv env -p python3
~$ source env/bin/activate
```

L'installation des dépendances est alors très simple.

```
~$ pip install -r requirements.txt
```

## 3.2 Mise en place de la base de données (BDD)

Nous ne détaillons pas ici l'installation d'une BDD. Le choix de la BDD est laissé libre à l'utilisateur.

Après avoir créé la BDD qui sera utilisé par Django pour stocker les données, il est nécessaire d'installer un module python qui fera le lien entre cette base de données et Django. Selon le type de BDD, le module n'est pas le même.

```
# MySQL ou MariaDB
~$ pip install mysqlclient

# PostgreSQL
~$ pip install psycopg2-binary
```

## 3.3 Variables d'environnement

Certaines variables d'environnement sont nécessaires au bon fonctionnement de l'application Django. Elles sont détaillées ci-dessous. Leur valeur par défaut sont aussi renseignées. Des exemples sont proposés ci-dessous.

```
# Clé secrète requise par Django
# Pour générer une nouvelle clé secrète, vous pouvez vous rendre sur le site :
# https://djecrety.ir/
MPC_DJANGO_SECRET_KEY : '=!_wwhyg-7t(*84mgib868m&qb(ix-vixko+g7=ymv$rwf2$n_'

# URL pour servir les fichiers statiques de Django
MPC_STATIC_URL : '/django_static/'

# Dossier servant à stocker les fichiers statiques de Django
MPC_STATIC_ROOT : '<workdir>/monpetitcarbone/backend/static/'

# Nom s'affichant lors de l'envoi d'un mail par le serveur
MPC_EMAIL_SENDER_NAME : 'webmaster@lab.fr'

# Indique si une connexion TLS (sécurisée)
# doit être utilisée pour le dialogue avec le serveur SMTP.
# Valeur : 'True' ou 'False'
MPC_EMAIL_USE_TLS: 'True'

# Indique si une connexion TLS implicite (sécurisée)
# doit être utilisée pour le dialogue avec le serveur SMTP.
# Attention, MPC_EMAIL_USE_SSL doit avoir une valeur
# contraire de MPC_EMAIL_USE_TLS. Valeur : 'True' ou 'False'
MPC_EMAIL_USE_SSL: 'False'

# Hôte du serveur mail
MPC_EMAIL_HOST: 'smtp.gmail.com'

# Port du serveur mail
MPC_EMAIL_PORT: 587

# Nom d'utilisateur de l'email utilisé
MPC_EMAIL_USER: 'your_account@gmail.com'

# Mot de passe de l'email utilisé
MPC_EMAIL_PASSWORD: 'password_email'

# Moteur ORM de Django utilisé selon le type de BDD.
# MySQL ou MariaDB : 'django.db.backends.mysql'
# PostgreSQL : 'django.db.backends.postgresql'
MPC_DB_ENGINE: 'django.db.backends.mysql'

# Nom de la BDD associée
MPC_DB_NAME: 'nom_bdd'

# Nom d'utilisateur associée à la BDD
MPC_DB_USER: 'user_bdd'

# Mot de passe de la BDD
MPC_DB_PASSWORD: 'password_bdd'

# Hôte de la BDD
MPC_DB_HOST: '127.0.0.1'

# Port de la BDD
MPC_DB_PORT: '3306'

# URL du serveur frontend configuré (seulement pour le mettre dans les mails)
URL_SERVER: 'http://localhost:8080'
```

Après avoir déclaré ces variables d'environnements, l'application Django devrait fonctionner. Essayez la commande suivante :

```
~$ python manage.py runserver
```

Si elle ne retourne pas d'erreurs, vous pouvez continuer.

### 3.4 Fichiers statiques django

Pour utiliser la partie administration de Django (/api/admin/) et les templates de Django Rest Framework, il est nécessaire de collecter les fichiers statiques.

```
~$ python manage.py collectstatic
```

Les fichiers sont normalement collectés et placés dans la variable d'environnement MPC_STATIC_ROOT.

### 3.5 Installation de gunicorn

La plate-forme principale de déploiement Django est WSGI, le standard Python pour les serveurs et applications Web. Cette particularité nécessite l'utilisateur d'une application tiers pour faire tourner l'application.

On utilise ici gunicorn.

```
~$ pip install gunicorn
```

La commande suivante permet de lancer l'application via le serveur gunicorn :

```
~$ gunicorn core.wsgi
```

gunicorn est maintenant capable de faire tourner l'application Django. Un logiciel tel que Supervisor peut être ensuite utilisé pour faire tourner de manière continue le processus gunicorn. Sa mise en place n'est pas détaillée ici mais vous trouvererez un tutoriel ici :

https://openclassrooms.com/en/courses/4425101-deployez-une-application-django/4688628-configurez-gunicorn-et-supervisor

Vous trouverez un exemple de fichier de configuration Supervisor ici : `./installation/without_docker/supervisor.basic.conf`. Il sera sûrement nécessaire de le modifier et de l'adapter à vos besoins pour qu'il fonctionne.

### 3.6 Finalisation de la configuration nginx

L'application Django est servie à l'application Vuejs via le chemin `/api/` au sein du même hôte virtuel établi via nginx. Ainsi, le bout de code suivant doit être rajouté au fichier de configuration de nginx de façon à ce que le serveur nginx puisse accéder à l'application Django :

Vous trouverez un exemple de fichier de configuration ici : `./installation/without_docker/nginx.basic.conf`

### 3.7 Compte administrateur

Django propose une interface d'administration accessible via `/api/admin`.
Pour créer un compte administrateur Django permettant d'accéder à cette interface, il suffit de lancer la commande suivante :

```
~$ cd workdir/monpetitcarbone/backend/
~$ python manage.py createsuperuser
```

Après avoir renseigné les informations de ce compte, vous pouvez vous connecter à la page administrateur. La page s'affiche normalement si les fichiers statiques Django ont été préalablement collectés (voir Section 3.4).
