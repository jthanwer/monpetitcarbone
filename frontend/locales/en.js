module.exports = {
  validation: {
    required: 'This field is required',
    integer: 'The number must be an integer',
    email: 'The email address is not valid',
    min: 'This field must contain at least {length} characters',
    min_value: 'The number must be greater than or equal to {min}',
    max_value: 'The number must be less than or equal to {max}',
    unique_email: 'This email address is already taken by another user',
    same_password: 'Passwords do not match',
    same_email: 'Email addresses do not match',
  },

  accueil: 'Home',
  calculateur: 'Calculator',
  vueDensemble: 'Overview',
  aPropos: 'About',
  methodologie: 'Methodology',
  ecrireRetour: 'Write a feedback',
  profil: 'Profile',
  administration: 'Admin',
  gestionCompte: 'User management',
  deconnexion: 'Logout',

  annee: 'Year',
  categories: 'Categories',
  missions: 'Travels',
  domicileTravail: 'Commutes',
  dateDepart: 'Departure date',
  empreinte: 'Footprint',
  empreinteCarbone: 'Carbon footprint',
  empreinteCarboneTotale: 'Total carbon footprint',
  distance: 'Distance',
  distanceTotale: 'Total distance',
  jours: 'days',

  total: 'Total',
  avion: 'Plane',
  metro: 'Subway',
  ferry: 'Ferry',
  voiture: 'Car',
  tramway: 'Tramway',
  train: 'Train',
  taxi: 'Cab',
  rer: 'RER',
  bus: 'Bus',
  marche: 'Walk',
  velo: 'Bicycle',
  veloElectrique: 'Electric bicycle',
  trottinetteElectrique: 'Electric scooter',
  deuxRouesMotorise: 'Motorcycle',

  modifierNom: 'Change the name',
  modifierDonnees: 'Update the data',
  copier: 'Copy',
  afficherDetails: 'Show details',

  formMissions: 'Travel form',
  formDT: 'Commute form',
  modeDeplacement: 'Transport mode',
  nombrePassagers: 'Number of passengers',
  lieuDepart: 'Departure location',
  villeDepart: 'Departure city',
  paysDepart: 'Departure country',
  lieuArrivee: 'Arrival location',
  villeArrivee: 'Arrival city',
  paysArrivee: 'Arrival country',
  depart: 'Departure',
  arrivee: 'Arrival',
  retour: 'Return',

  seConnecter: 'Login',
  creerUnCompte: 'Register',
  adresseEmail: 'Email',
  adresseEmailConf: 'Email confirmation',
  mdp: 'Password',
  mdpConf: 'Password confirmation',
  oubliMdp: 'Did you forget your password ?',
  connexion: 'Login',
  mentionsLegales: 'Legal Notice',
  politiqueConfidentialite: 'Privacy Policy',

  identite: 'Identity',

  valider: 'Submit',
  confirmer: 'Confirm',
  enregistrer: 'Save',
  annuler: 'Cancel',
  supprimer: 'Delete',
  exporter: 'Export',
  reessayer: 'Try again',
  chargement: 'Loading...',
  oui: 'Yes',
  non: 'No',
}
