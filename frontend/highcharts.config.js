const theme = {
  colors: [
    '#A13C50',
    '#66C2A5',
    '#FC8D62',
    '#8DA0CB',
    '#6A5486',
    '#A6D854',
    '#FFD92F',
    '#E5C494',
    '#1A85B9',
  ],
  chart: {},
  title: {
    style: {
      color: '#000',
      font: 'bold 2em "Trebuchet MS", Verdana, sans-serif',
    },
  },
  subtitle: {
    style: {
      color: '#666666',
      font: 'bold 14px "Trebuchet MS", Verdana, sans-serif',
    },
  },
  xAxis: {
    title: {
      style: {
        font: 'bold 14px "Trebuchet MS", Verdana, sans-serif',
        color: '#000',
        fontSize: '1.5em',
      },
    },
    labels: {
      style: {
        fontSize: '1.3em',
      },
    },
  },
  yAxis: {
    title: {
      style: {
        font: 'bold 14px "Trebuchet MS", Verdana, sans-serif',
        color: '#000',
        fontSize: '1.5em',
      },
    },
    labels: {
      style: {
        fontSize: '1.3em',
      },
    },
  },
  legend: {
    itemStyle: {
      font: '9pt Trebuchet MS, Verdana, sans-serif',
      color: 'black',
    },
    itemHoverStyle: {
      color: 'gray',
    },
  },
}

module.exports = theme
