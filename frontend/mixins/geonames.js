import axios from 'axios'
export const geonames = {
  data() {
    return {
      geonamesAccount: 'joelthanwerdas',
    }
  },
  methods: {
    searchGEONames(
      ville,
      pays = '',
      maxRows = 2,
      fuzzy = 0.6,
      style = 'SHORT'
    ) {
      let request = 'https://secure.geonames.org/searchJSON?name=' + ville
      if (pays) {
        if (Array.isArray(pays)) {
          for (const cpays of pays) {
            request += '&country=' + cpays
          }
        } else {
          request += '&country=' + pays
        }
      }
      // request += '&countryBias=FR'
      request +=
        '&maxRows=' +
        maxRows +
        '&fuzzy=' +
        fuzzy +
        '&lang=fr&username=' +
        this.geonamesAccount +
        '&style=' +
        style
      return axios.get(request)
    },
  },
}
