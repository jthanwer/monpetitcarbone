import webpack from 'webpack'
import highchartsTheme from './highcharts.config.js'

export default {
  ssr: false,

  loading: '~/components/Loading.vue',

  server: {
    port: 8080,
  },

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'MonPetitCarbone - Suivez votre empreinte carbone professionnelle',
    htmlAttrs: {
      lang: 'fr',
    },
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: ''
      },
    ],
    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: '/favicon.ico'
    }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~/assets/scss/main.scss'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/i18n',
    '~/plugins/vee-validate',
    '~/plugins/services',
    '~plugins/filters',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    ['@nuxtjs/eslint-module', {
      fix: true
    }],
    '@nuxtjs/moment',
    'nuxt-compress',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/buefy
    ['nuxt-buefy', {
      css: false
    }],
    ['nuxt-i18n', {
      vueI18nLoader: true
    }],
    'nuxt-highcharts',
    'nuxt-leaflet',
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
    '@nuxtjs/proxy',
  ],

  axios: {
    proxy: true,
  },

  proxy: {
    '/api/': {
      target: 'http://0.0.0.0:8000/',
    },
  },

  auth: {
    redirect: {
      login: '/',
      logout: '/',
      callback: '/',
      home: '/',
    },
    cookie: false,
    plugins: ['~plugins/axios'],
    strategies: {
      local: {
        token: {
          property: 'access',
          required: true,
          type: 'Bearer',
        },
        user: {
          property: false,
        },
        endpoints: {
          login: {
            url: 'api/token/',
            method: 'post',
          },
          user: {
            url: 'api/users/my_profile/',
            method: 'get',
          },
          logout: false,
        },
      },
    },
  },

  router: {
    middleware: ['custom-auth'],
  },

  highcharts: {
    setOptions: highchartsTheme,
  },

  i18n: {
    langDir: '~/locales/',
    locales: [{
        code: 'fr',
        name: 'Français',
        iso: 'fr-FR',
        file: 'fr.js',
      },
      {
        code: 'en',
        name: 'English',
        iso: 'en-US',
        file: 'en.js',
      },
    ],
    defaultLocale: 'fr',
    vueI18n: {
      fallbackLocale: 'fr',
      silentFallbackWarn: true,
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    filenames: {
      app: ({
          isDev
        }) =>
        isDev ? '[name].js' : '[name].[id].[contenthash].js',
      chunk: ({
        isDev
      }) => (isDev ? '[name].js' : '[id].[contenthash].js'),
    },
    plugins: [
      new webpack.IgnorePlugin({
        resourceRegExp: /@highcharts\/map-collection/,
      }),
    ],
  },
}
