import facteursEmissions from '@/../data/facteursEmissions.json'

const modesDeplacement = [
  'marche',
  'velo',
  'veloElectrique',
  'trottinetteElectrique',
  'deuxRouesMotorise',
  'voiture',
  'bus',
  'tramway',
  'train',
  'rer',
  'metro',
]

export default class DeplacementDT {
  constructor(deplacementdt) {
    this.id = deplacementdt.id
    this.annee = deplacementdt.annee
    this.nomVoyage = deplacementdt.nomVoyage
    this.isAllerRetour = deplacementdt.isAllerRetour
    this.nbJoursTravail = deplacementdt.nbJoursTravail
    this.marche = deplacementdt.marche
    this.velo = deplacementdt.velo
    this.veloElectrique = deplacementdt.veloElectrique
    this.trottinetteElectrique = deplacementdt.trottinetteElectrique
    this.deuxRouesMotorise = deplacementdt.deuxRouesMotorise
    this.voiture = deplacementdt.voiture
    this.bus = deplacementdt.bus
    this.tramway = deplacementdt.tramway
    this.train = deplacementdt.train
    this.rer = deplacementdt.rer
    this.metro = deplacementdt.metro
    this.nbPersonnes2roues = deplacementdt.nbPersonnes2roues
    this.nbPersonnesVoiture = deplacementdt.nbPersonnesVoiture
    this.motorisation = deplacementdt.motorisation
    this.tailleAgglomeration = deplacementdt.tailleAgglomeration
  }

  getFacteurEmission(modeDeplacement) {
    let fe = null
    if (modeDeplacement === 'marche') {
      fe = {
        categorie: 'Transport en commun',
        souscategorie: 'Marche',
        energietype: 'Musculaire',
        unite: 'km',
        nomBaseCarbone: '',
        decomposition: [
          {
            nom: 'Amont et combustion',
            co2: 0.0,
            ch4: 0.0,
            n2o: 0.0,
            autres: 0.0,
            unite: 'kgCO2e/passager.km',
            total: 0.0,
            incertitude: 0.0,
          },
        ],
      }
    } else if (modeDeplacement === 'velo') {
      fe = facteursEmissions
        .filter((obj) => obj.souscategorie === 'Vélo')
        .filter((obj) => obj.energietype === 'Musculaire')[0]
    } else if (modeDeplacement === 'veloElectrique') {
      fe = facteursEmissions
        .filter((obj) => obj.souscategorie === 'Vélo')
        .filter((obj) => obj.energietype === 'Electrique')[0]
    } else if (modeDeplacement === 'trottinetteElectrique') {
      fe = facteursEmissions.filter(
        (obj) => obj.souscategorie === 'Trottinette'
      )[0]
    } else if (modeDeplacement === 'deuxRouesMotorise') {
      fe = facteursEmissions.filter((obj) => obj.souscategorie === 'Moto')[0]
    } else if (modeDeplacement === 'voiture') {
      fe = facteursEmissions.filter((obj) => obj.souscategorie === 'Voiture')
      fe = fe.filter((obj) => obj.energietype === this.motorisation)
      fe = fe.filter((obj) => obj.unite === 'km')[0]
    } else if (modeDeplacement === 'bus') {
      if (this.tailleAgglomeration === 0) {
        fe = facteursEmissions.filter(
          (obj) =>
            obj.nomBaseCarbone ===
            'Autobus moyen - Agglomération moins de 100 000 habitants'
        )[0]
      } else if (this.tailleAgglomeration === 1) {
        fe = facteursEmissions.filter(
          (obj) =>
            obj.nomBaseCarbone ===
            'Autobus moyen - Agglomération de 100 000 à 250 000 habitants'
        )[0]
      } else {
        fe = facteursEmissions.filter(
          (obj) =>
            obj.nomBaseCarbone ===
            'Autobus moyen - Agglomération de plus de 250 000 habitants'
        )[0]
      }
    } else if (modeDeplacement === 'train') {
      if (this[modeDeplacement] < 200) {
        fe = facteursEmissions.filter(
          (obj) => obj.energietype === 'Train <200 km'
        )[0]
      } else {
        fe = facteursEmissions.filter(
          (obj) => obj.energietype === 'TGV > 200 km'
        )[0]
      }
    } else if (modeDeplacement === 'tramway') {
      if (this.tailleAgglomeration === 0) {
        fe = facteursEmissions.filter(
          (obj) => obj.energietype === 'Tramway =< 250 000 hab'
        )[0]
      } else if (this.tailleAgglomeration === 1) {
        fe = facteursEmissions.filter(
          (obj) => obj.energietype === 'Tramway =< 250 000 hab'
        )[0]
      } else {
        fe = facteursEmissions.filter(
          (obj) => obj.energietype === 'Tramway > 250 000 hab'
        )[0]
      }
    } else if (modeDeplacement === 'rer') {
      fe = facteursEmissions.filter((obj) => obj.energietype === 'RER')[0]
    } else if (modeDeplacement === 'metro') {
      fe = facteursEmissions.filter((obj) => obj.energietype === 'Métro')[0]
    }
    return fe
  }

  getEmissionModeDeplacement(modeDeplacement) {
    let carbone = 0
    const distance = this[modeDeplacement]
    if (distance) {
      let fe = this.getFacteurEmission(modeDeplacement)
      fe = fe.decomposition[0].total
      if (modeDeplacement === 'voiture') {
        fe = fe / this.nbPersonnesVoiture
      } else if (modeDeplacement === 'deuxRouesMotorise') {
        fe = fe / this.nbPersonnes2roues
      }
      carbone += fe * distance
    }
    if (this.isAllerRetour) {
      carbone *= 2
    }
    return carbone * this.nbJoursTravail
  }

  get carbone() {
    let carbone = 0
    for (const modeDeplacement of modesDeplacement) {
      carbone += this.getEmissionModeDeplacement(modeDeplacement)
    }
    return carbone
  }

  get distance() {
    let distance = 0
    for (const modeDeplacement of modesDeplacement) {
      const value = this[modeDeplacement]
      if (value) {
        distance += value
      }
    }
    if (this.isAllerRetour) {
      distance *= 2
    }
    return distance * this.nbJoursTravail
  }
}
