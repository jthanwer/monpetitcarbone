import SectionMission from '@/models/SectionMission'

export default class Mission {
  constructor(mission) {
    this.id = mission.id
    this.nomVoyage = mission.nomVoyage
    this.dateDepart = mission.dateDepart
    this.dateRetour = mission.dateRetour
    this.isAllerRetour = mission.isAllerRetour
    this.dateCreation = mission.dateCreation
    this.sections = mission.sections.map((obj) => new SectionMission(obj))
  }

  get carbone() {
    let carbone = 0
    for (const section of this.sections) {
      carbone += section.carbone
    }
    if (this.isAllerRetour) {
      carbone *= 2
    }
    return carbone
  }

  get distance() {
    let distance = 0
    for (const section of this.sections) {
      distance += section.distance
    }
    if (this.isAllerRetour) {
      distance *= 2
    }
    return distance
  }
}
