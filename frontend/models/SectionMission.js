import facteursEmissions from '@/../data/facteursEmissions.json'

const modesDeplacements = {
  Avion: 1.0,
  Train: 1.2,
  Voiture: 1.3,
  Taxi: 1.3,
  Bus: 1.5,
  Tramway: 1.5,
  RER: 1.2,
  Métro: 1.7,
  Ferry: 1.0,
}

export default class SectionMission {
  constructor(section) {
    this.id = section.id
    this.modeDeplacement = section.modeDeplacement
    this.villeDepart = section.villeDepart
    this.paysDepart = section.paysDepart
    this.villeDestination = section.villeDestination
    this.paysDestination = section.paysDestination
    this.nbPersonneVoiture = section.nbPersonneVoiture
    this.distance = section.distance
  }

  get carbone() {
    return this.getEmissions()
  }

  get missionType() {
    let missionType = 'IN'
    if (this.paysDepart == 'FR' && this.paysDestination == 'FR') {
      missionType = 'NA'
    } else if (this.paysDepart == 'FR' || this.paysDestination == 'FR') {
      missionType = 'MX'
    }
    return missionType
  }

  getEmissions() {
    let cdistance = this.distance
    const fe = this.getFacteurEmission(this.distance)
    if (this.modeDeplacement == 'Avion') {
      cdistance += 95
    } else if (this.modeDeplacement == 'Taxi') {
      cdistance = cdistance * (1 + 1 / this.nbPersonneVoiture)
    } else if (this.modeDeplacement == 'Voiture') {
      cdistance = cdistance / this.nbPersonneVoiture
    }
    if (this.isAllerRetour) {
      cdistance = cdistance * 2
    }
    cdistance = cdistance * modesDeplacements[this.modeDeplacement]
    return cdistance * fe.decomposition[0].total
  }

  getFacteurEmission(distance) {
    let fe = null
    if (this.modeDeplacement == 'Avion') {
      fe = facteursEmissions.filter((obj) => obj.souscategorie == 'Avion')
      if (distance < 1000) {
        fe = fe.filter((obj) => obj.energietype == '<1000 km')[0]
      } else if (distance < 3501) {
        fe = fe.filter((obj) => obj.energietype == '1001-3500km')[0]
      } else {
        fe = fe.filter((obj) => obj.energietype == '>3500 km')[0]
      }
    } else if (
      this.modeDeplacement == 'Voiture' ||
      this.modeDeplacement == 'Taxi'
    ) {
      fe = facteursEmissions.filter((obj) => obj.souscategorie == 'Voiture')
      fe = fe.filter((obj) => obj.energietype == 'Motorisation inconnue')[0]
    } else if (this.modeDeplacement == 'Bus') {
      fe = facteursEmissions.filter(
        (obj) =>
          obj.nomBaseCarbone ==
          'Autobus moyen - Agglomération de plus de 250 000 habitants'
      )[0]
    } else if (this.modeDeplacement == 'Train') {
      if (this.missionType == 'NA') {
        if (distance < 200) {
          fe = facteursEmissions.filter(
            (obj) => obj.energietype == 'Train <200 km'
          )[0]
        } else {
          fe = facteursEmissions.filter(
            (obj) => obj.energietype == 'TGV > 200 km'
          )[0]
        }
      } else if (this.missionType == 'MX') {
        fe = facteursEmissions.filter(
          (obj) => obj.energietype == 'Train mixte France et international'
        )[0]
      } else {
        fe = facteursEmissions.filter(
          (obj) => obj.energietype == 'Train international'
        )[0]
      }
    } else if (this.modeDeplacement == 'Tramway') {
      fe = facteursEmissions.filter(
        (obj) => obj.energietype == 'Tramway > 250 000 hab'
      )[0]
    } else if (this.modeDeplacement == 'RER') {
      fe = facteursEmissions.filter((obj) => obj.energietype == 'RER')[0]
    } else if (this.modeDeplacement == 'Métro') {
      fe = facteursEmissions.filter((obj) => obj.energietype == 'Métro')[0]
    } else if (this.modeDeplacement == 'Ferry') {
      fe = facteursEmissions.filter((obj) => obj.energietype == 'Ferry')[0]
    }
    return fe
  }

  static geodesicDist(lat1, lon1, lat2, lon2) {
    if (lat1 == lat2 && lon1 == lon2) {
      return 0
    } else {
      const radlat1 = (Math.PI * lat1) / 180
      const radlat2 = (Math.PI * lat2) / 180
      const theta = lon1 - lon2
      const radtheta = (Math.PI * theta) / 180
      let dist =
        Math.sin(radlat1) * Math.sin(radlat2) +
        Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta)
      if (dist > 1) {
        dist = 1
      }
      dist = Math.acos(dist)
      dist = (dist * 180) / Math.PI
      dist = dist * 60 * 1.1515
      dist = dist * 1.609344
      return dist
    }
  }
}
