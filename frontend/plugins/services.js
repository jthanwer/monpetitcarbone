import createUserService from '~/services/users'
import createMissionService from '~/services/missions'
import createDeplacementDTService from '~/services/deplacementsdt'
import createVisualisationService from '~/services/visualisation'
export default (ctx, inject) => {
  inject('usersService', createUserService(ctx.$axios))
  inject('missionService', createMissionService(ctx.$axios))
  inject('deplacementdtService', createDeplacementDTService(ctx.$axios))
  inject('visualisationService', createVisualisationService(ctx.$axios))
}
