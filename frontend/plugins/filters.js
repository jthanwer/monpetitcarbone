import Vue from 'vue'
import range from '@/utils/range.js'

Vue.filter('splitNumber', function (value) {
  return value
    .toFixed(0)
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, ' ')
})

Vue.filter('splitNumberTons', function (value) {
  return (value / 1000)
    .toFixed(1)
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, ' ')
})

const yearBegin = 2010
const yearEnd = 2030
Vue.prototype.$time_window = range(yearBegin, yearEnd + 1)
