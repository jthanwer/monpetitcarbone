import { ToastProgrammatic as Toast } from 'buefy'

export default function ({ $axios, $auth, redirect }) {
  $axios.interceptors.response.use(
    function (response) {
      return response
    },
    function (error) {
      const tokenStatus = $auth.strategy.token.status()
      const code = parseInt(error.response && error.response.status)
      if (code === 401) {
        redirect('/')
        if (!tokenStatus.valid()) {
          Toast.open({
            duration: 5000,
            message: `Votre session est terminée ! Vous avez été déconnecté.`,
          })
        }
      }

      return Promise.reject(error)
    }
  )
}
