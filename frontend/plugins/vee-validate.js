/* eslint-disable camelcase */
import Vue from 'vue'
import {
  ValidationProvider,
  ValidationObserver,
  extend,
  setInteractionMode,
} from 'vee-validate'
import {
  required,
  min,
  min_value,
  max_value,
  email,
  integer,
} from 'vee-validate/dist/rules'
// import { extend, setInteractionMode, localize } from 'vee-validate'
// import fr from 'vee-validate/dist/locale/fr.json'
import axios from 'axios'

// localize('fr', fr)

Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)

setInteractionMode('lazy')

const isUnique = function (type) {
  return (value) =>
    new Promise((resolve) => {
      const payload = {}
      payload[type] = value
      axios.post('api/users/check_credentials/', payload).then((response) => {
        if (response.data.unique) {
          return resolve({
            valid: true,
          })
        }

        return resolve({
          valid: false,
        })
      })
    })
}

extend('required', {
  ...required,
  // message: 'Ce champ est requis',
})

extend('integer', {
  ...integer,
})

extend('email', {
  ...email,
})

extend('min', {
  ...min,
})

extend('min_value', {
  ...min_value,
})

extend('max_value', {
  ...max_value,
})

extend('unique_email', {
  validate: isUnique('email'),
})

extend('same_password', {
  params: ['target'],
  validate(value, { target }) {
    return value === target
  },
})

extend('same_email', {
  params: ['target'],
  validate(value, { target }) {
    return value === target
  },
})
