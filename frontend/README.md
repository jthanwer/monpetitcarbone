# frontend

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload (proxy working -- use this for dev)
$ npm run dev

# build for production and launch server
$ npm run generate
$ npm run start

$ npm install <package> --save-dev

# Create a Docker image
$ docker build . -t mpc-frontend -f .\frontend\frontend.dockerfile
$ docker build . -t mpc-backend -f .\backend\backend.dockerfile
$ docker run -d --name frontend-cont mpc-frontend
$ docker exec -it frontend-cont sh -c "echo a && echo b"
$ docker-compose up --build
```
