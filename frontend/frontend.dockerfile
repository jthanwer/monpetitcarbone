# --------------
# -- Frontend
# --------------
FROM node:16.16.0-alpine AS frontend

COPY data /app/data
WORKDIR /app/frontend
COPY frontend/package*.json ./
RUN npm install
COPY frontend ./
RUN npm run generate

# --------------
# -- Nginx
# --------------
# FROM nginxinc/nginx-unprivileged:latest as nginx
FROM nginx:latest as nginx
RUN rm /etc/nginx/conf.d/default.conf
COPY ./nginx.docker.conf /etc/nginx/conf.d/default.conf
COPY --from=frontend /app/frontend /app/frontend
