import moment from 'moment'
export default ($axios) => ({
  getMissions(annee, pageNumber) {
    const endpoint = `api/mission/?annee=${annee}&page=${pageNumber}`
    return $axios.$get(endpoint)
  },
  postMission(data) {
    return $axios.$post('api/mission/', data)
  },
  updateMission(id, data) {
    return $axios.$patch(`api/mission/${id}/`, data)
  },
  deleteMission(id) {
    return $axios.$delete(`api/mission/${id}/`)
  },
  getCarboneMission(data) {
    return $axios.$post(`api/mission/carbone/`, data)
  },
  extractionDonneesMissionsClassique() {
    return $axios({
      url: `api/export_missions_user/?format=csv`,
      method: 'GET',
      responseType: 'blob',
    }).then(({ data }) => {
      const downloadUrl = window.URL.createObjectURL(new Blob([data]))
      const link = document.createElement('a')
      link.href = downloadUrl
      link.setAttribute(
        'download',
        'donneesmissions_' + moment().format('YYYYMMDD') + '.csv'
      )
      document.body.appendChild(link)
      link.click()
      link.remove()
    })
  },
  extractionDonneesMissionsLabos1p5() {
    return $axios({
      url: `api/export_missions_user_labos1p5/?format=csv`,
      method: 'GET',
      responseType: 'blob',
    }).then(({ data }) => {
      const downloadUrl = window.URL.createObjectURL(new Blob([data]))
      const link = document.createElement('a')
      link.href = downloadUrl
      link.setAttribute(
        'download',
        'donneesmissionsLabos1p5_' + moment().format('YYYYMMDD') + '.csv'
      )
      document.body.appendChild(link)
      link.click()
      link.remove()
    })
  },
})
