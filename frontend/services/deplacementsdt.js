export default ($axios) => ({
  getDeplacementsDT(annee) {
    return $axios.$get(`api/deplacementdt/?annee=${annee}`)
  },
  postDeplacementDT(data) {
    return $axios.$post('api/deplacementdt/', data)
  },
  updateDeplacementDT(id, data) {
    return $axios.$patch(`api/deplacementdt/${id}/`, data)
  },
  deleteDeplacementDT(id) {
    return $axios.$delete(`api/deplacementdt/${id}/`)
  },
  getCarboneDeplacementDT(data) {
    return $axios.$post(`api/deplacementdt/carbone/`, data)
  },
})
