export default ($axios) => ({
  getChartData() {
    return $axios.get(`api/chart/data/`)
  },
})
