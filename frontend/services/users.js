export default ($axios) => ({
  authenticate(user) {
    return $axios.$post('api/token/', user)
  },
  register(user) {
    return $axios.$post('api/users/', user)
  },
  get_profile() {
    return $axios.$get('api/users/my_profile/')
  },
  update_profile(id, payload) {
    return $axios.$patch(`api/users/${id}/`, payload)
  },
  check_credentials(payload) {
    return $axios.$post('api/users/check_credentials/', payload)
  },
  change_password(payload) {
    return $axios.$post('api/users/change_password/', payload)
  },
  reset_password(payload) {
    return $axios.$post('api/accounts/password/reset/', payload)
  },
  send_feedback(payload) {
    return $axios.$post('api/users/send_feedback/', payload)
  },
})
