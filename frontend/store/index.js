import moment from 'moment'

export const state = () => ({
  annee: moment().year(),
})

export const getters = {
  isAuthenticated: (state) => state.auth.loggedIn,
  isStaff: (state) => (state.auth.user ? state.auth.user.is_staff : false),
}

export const mutations = {
  INCREASE_YEAR(state, amount = 1) {
    state.annee += amount
  },
  DECREASE_YEAR(state, amount = -1) {
    state.annee += amount
  },
  UPDATE_YEAR(state, annee) {
    state.annee = annee
  },
  SET_USER(state, user) {
    state.auth.setUser(user)
  },
}

export const actions = {
  async updateYear({ state, commit, dispatch }, annee) {
    commit('UPDATE_YEAR', annee)
    await dispatch(
      'missions/getMissions',
      {
        annee: state.annee,
        pageNumber: 1,
      },
      { root: true }
    )
    await dispatch('deplacementsdt/getDeplacementsDT', state.annee, {
      root: true,
    })
  },
  updateProfileUser({ commit }, updatedUser) {
    commit('SET_USER', updatedUser)
  },
}
