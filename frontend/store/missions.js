import Mission from '@/models/Mission'
import moment from 'moment'

const orderFunction = function (keyOrder, valueOrder) {
  if (keyOrder === 'dateDepart') {
    if (valueOrder === true) {
      return (a, b) => new Date(a[keyOrder]) - new Date(b[keyOrder])
    }
    return (a, b) => new Date(b[keyOrder]) - new Date(a[keyOrder])
  } else {
    if (valueOrder === true) {
      return (a, b) => a[keyOrder] - b[keyOrder]
    }
    return (a, b) => b[keyOrder] - a[keyOrder]
  }
}

export const state = () => ({
  isLoadingMissions: false,
  missions: [],
  countMissions: 0,
})

const getters = {}

const mutations = {
  LOADING_MISSIONS(state) {
    state.isLoadingMissions = true
  },
  SET_MISSIONS_ANNEE(state, payload) {
    const { missions, countMissions } = payload
    state.missions = missions
    state.countMissions = countMissions
    state.isLoadingMissions = false
  },
  ADD_MISSION(state, mission) {
    state.missions.unshift(mission)
  },
  UPDATE_MISSION(state, mission) {
    const index = state.missions.findIndex((obj) => obj.id === mission.id)
    state.missions.splice(index, 1, mission)
  },
  DELETE_MISSION(state, mission) {
    const index = state.missions.findIndex((obj) => obj.id === mission.id)
    state.missions.splice(index, 1)
  },
  SORT_MISSION(state, { keyOrder, valueOrder }) {
    state.missions.sort(orderFunction(keyOrder, valueOrder))
  },
}

const actions = {
  async getMissions({ commit }, payload) {
    const annee = payload.annee
    const pageNumber = payload.pageNumber
    commit('LOADING_MISSIONS')
    const data = await this.$missionService.getMissions(annee, pageNumber)
    const missions = data.results.map((obj) => new Mission(obj))
    const countMissions = data.count
    commit('SET_MISSIONS_ANNEE', { missions, countMissions })
    return data
  },
  async addMission({ commit, rootState }, mission) {
    const data = await this.$missionService.postMission(mission)
    if (rootState.annee === moment(mission.dateDepart).year()) {
      commit('ADD_MISSION', new Mission(data))
    }
    return data
  },
  async updateMission({ commit, rootState }, mission) {
    const clone = JSON.parse(JSON.stringify(mission))
    delete clone.sections
    const data = await this.$missionService.updateMission(clone.id, clone)
    if (rootState.annee === moment(data.dateDepart).year()) {
      commit('UPDATE_MISSION', new Mission(data))
    } else {
      commit('DELETE_MISSION', clone.id)
    }
    return data
  },
  async deleteMission({ commit }, mission) {
    const data = await this.$missionService.deleteMission(mission.id)
    commit('DELETE_MISSION', mission)
    return data
  },
  sortMission({ commit }, { keyOrder, valueOrder }) {
    commit('SORT_MISSION', { keyOrder, valueOrder })
  },
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
