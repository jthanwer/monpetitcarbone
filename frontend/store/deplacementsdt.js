import DeplacementDT from '@/models/DeplacementDT'

const orderFunction = function (keyOrder, valueOrder) {
  if (keyOrder === 'dateDepart') {
    if (valueOrder === true) {
      return (a, b) => new Date(a[keyOrder]) - new Date(b[keyOrder])
    }
    return (a, b) => new Date(b[keyOrder]) - new Date(a[keyOrder])
  } else {
    if (valueOrder === true) {
      return (a, b) => a[keyOrder] - b[keyOrder]
    }
    return (a, b) => b[keyOrder] - a[keyOrder]
  }
}

export const state = () => ({
  isLoadingDeplacementsDT: false,
  deplacementsdt: [],
})

export const getters = {}

export const mutations = {
  LOAD_DEPLACEMENTSDT(state) {
    state.isLoadingDeplacementsDT = true
  },
  SET_DEPLACEMENTSDT_ANNEE(state, deplacementsdt) {
    state.deplacementsdt = deplacementsdt
    state.isLoadingDeplacementsDT = false
  },
  ADD_DEPLACEMENTDT(state, deplacementdt) {
    state.deplacementsdt.unshift(deplacementdt)
  },
  UPDATE_DEPLACEMENTDT(state, deplacementdt) {
    const index = state.deplacementsdt.findIndex(
      (obj) => obj.id === deplacementdt.id
    )
    state.deplacementsdt.splice(index, 1, deplacementdt)
  },
  DELETE_DEPLACEMENTDT(state, deplacementdt) {
    const index = state.deplacementsdt.findIndex(
      (obj) => obj.id === deplacementdt.id
    )
    state.deplacementsdt.splice(index, 1)
  },
  SORT_DEPLACEMENTSDT(state, { keyOrder, valueOrder }) {
    state.deplacementsdt.sort(orderFunction(keyOrder, valueOrder))
  },
}

export const actions = {
  async getDeplacementsDT({ commit }, annee) {
    const data = await this.$deplacementdtService.getDeplacementsDT(annee)
    const deplacementsdt = data.results.map((obj) => new DeplacementDT(obj))
    commit('SET_DEPLACEMENTSDT_ANNEE', deplacementsdt)
    return data
  },
  async addDeplacementDT({ commit, rootState }, deplacementdt) {
    const data = await this.$deplacementdtService.postDeplacementDT(
      deplacementdt
    )
    if (rootState.annee === deplacementdt.annee) {
      commit('ADD_DEPLACEMENTDT', new DeplacementDT(data))
    }
    return data
  },
  async updateDeplacementDT({ commit, rootState }, deplacementdt) {
    const clone = JSON.parse(JSON.stringify(deplacementdt))
    const data = await this.$deplacementdtService.updateDeplacementDT(
      clone.id,
      clone
    )
    if (rootState.annee === data.annee) {
      commit('UPDATE_DEPLACEMENTDT', new DeplacementDT(data))
    } else {
      commit('DELETE_DEPLACEMENTDT', clone.id)
    }
    return data
  },
  async deleteDeplacementDT({ commit }, deplacementdt) {
    const data = await this.$deplacementdtService.deleteDeplacementDT(
      deplacementdt.id
    )
    commit('DELETE_DEPLACEMENTDT', deplacementdt)
    return data
  },
  sortDeplacementsDT({ state, commit }, { keyOrder, valueOrder }) {
    commit('SORT_DEPLACEMENTSDT', { keyOrder, valueOrder })
  },
}
