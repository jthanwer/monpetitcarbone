import Vue from "vue";

Vue.filter("splitNumber", function(value) {
  return value
    .toFixed(0)
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, " ");
});

Vue.filter("splitNumberTons", function(value) {
  return (value / 1000)
    .toFixed(1)
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, " ");
});
