import Vue from "vue";
import range from "@/utils/range.js";

const year_begin = 2010;
const year_end = 2030;
Vue.prototype.$time_window = range(year_begin, year_end + 1);
