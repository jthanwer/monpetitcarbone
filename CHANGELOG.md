# MonPetitCarbone Changelog

## 1.1

### Nouvelles fonctionnalités

#### Général

* Possibilité de faire un retour sur l'expérience utilisateur
* Amélioration de la performance de l'outil via l'utilisation d'images optimisées

#### Calculateur

* Ajout d'un court délai entre le changement d'année et la récupération de données pour réduire le nombre de requêtes effectuées.

#### Missions

* Changement de forme du bouton "Ajouter une mission"
* Ajout d'une carte LeafLet afin de visualiser les différentes sections
* Calcul en temps réel de la distance et des émissions de la mission en train d'être renseignée
* Possibilité de copier une mission
* Possibilité de modifier certaines données d'une mission (nom, date de départ, date de retour, trajet aller-retour)


### Corrections de bugs

* Le facteur d'émission du train n'était pas le bon pour les trajets effectués en France. Les missions déjà rentrées dans l'outil ont été mises à jour automatiquement. 
