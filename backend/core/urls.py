"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path, re_path
from django.http import JsonResponse
from django.conf import settings

from rest_framework.response import Response
from rest_framework.decorators import api_view

from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
import carbon_indi.views as cv

if settings.DEBUG:
    urlpatterns = [
        # -- Admin
        path('api/admin/', admin.site.urls),

        # -- Export missions
        path('api/export_missions_labos1p5/', cv.export_missions_labos1p5, name='export_missions_labos1p5'),
        path('api/export_missions_user/', cv.export_missions_user, name='export_missions_user'),
        path('api/export_missions_user_labos1p5/', cv.export_missions_user_labos1p5,
             name='export_missions_user_labos1p5'),

        # -- JWT Token
        path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
        path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

        # -- API
        path('api/', include('users.urls')),
        path('api/', include('carbon_indi.urls'))
    ]

else:
    urlpatterns = [
        # -- Admin
        path('admin/', admin.site.urls),

        # -- Export missions
        path('export_missions_labos1p5/', cv.export_missions_labos1p5, name='export_missions_labos1p5'),
        path('export_missions_user/', cv.export_missions_user, name='export_missions_user'),
        path('export_missions_user_labos1p5/', cv.export_missions_user_labos1p5, name='export_missions_user_labos1p5'),

        # -- JWT Token
        path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
        path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

        # -- API
        path('', include('users.urls')),
        path('', include('carbon_indi.urls'))
    ]
