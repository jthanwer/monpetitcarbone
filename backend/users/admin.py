from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import CustomUser
from .forms import CustomUserForm, CustomUserChangeForm


class CustomUserAdmin(UserAdmin):
    form = CustomUserChangeForm
    add_form = CustomUserForm

    list_display = ('email', 'is_active', 'date_joined')
    list_filter = ('is_staff',)
    fieldsets = (
        (None, {'fields': ('email', 'is_active', 'password')}),
        ('Permissions', {'fields': ('is_staff',)}),
    )

    add_fieldsets = (
        (None, {
            'fields': ('email', 'password1', 'password2')}
         ),
    )
    search_fields = ('email',)
    ordering = ('-date_joined',)
    filter_horizontal = ()


admin.site.register(CustomUser, CustomUserAdmin)

