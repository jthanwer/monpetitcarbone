from django.db import models
from django.db.models import Sum, F
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin, AbstractUser
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from django.conf import settings
from .managers import UserManager
from carbon_indi.carbone import get_deplacementdt_mode_emission
from carbon_indi.models import SectMission, Mission, DeplacementDT


class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField('email', max_length=150, unique=True,
                              error_messages={
                                  'unique': _("A user with that email already exists.")
                              })
    date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)
    is_staff = models.BooleanField(_('staff status'), default=False)
    is_active = models.BooleanField(_('active'), default=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'

    class Meta:
        verbose_name = 'user'

    def __str__(self):
        return '{}'.format(self.email)

    # -- Missions
    def missionSubCat(self, modeDep, annee):
        qs = SectMission.objects.filter(modeDeplacement=modeDep,
                                        mission__dateDepart__year=annee,
                                        mission__user=self)

        qs_nar = qs.filter(mission__isAllerRetour=False)
        qs_ar = qs.filter(mission__isAllerRetour=True)
        carbone = sum([section.carbone for section in qs_nar])
        carbone += sum([section.carbone * 2 for section in qs_ar])
        return carbone

    def mission(self, annee):
        qs = Mission.objects.filter(dateDepart__year=annee,
                                    user=self)
        return sum([mission.carbone for mission in qs])

    # -- Deplacements Domicile-Travail
    def deplacementdtSubCat(self, modeDep, annee):
        qs = DeplacementDT.objects.filter(annee=annee, user=self)
        return sum([get_deplacementdt_mode_emission(deplacementdt, modeDep) for deplacementdt in qs])

    def deplacementdt(self, annee):
        qs = DeplacementDT.objects.filter(annee=annee, user=self)
        return sum([deplacementdt.carbone for deplacementdt in qs])

    # -- Consommation Totale
    def consommationTotale(self, annee):
        return self.mission(annee) + self.deplacementdt(annee)
