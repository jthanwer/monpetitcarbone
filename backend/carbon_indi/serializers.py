from rest_framework import serializers

from django.conf import settings

from carbon_indi.models import SectMission, Mission, DeplacementDT
from carbon_indi.carbone import *


class SectMissionSerializer(serializers.ModelSerializer):

    class Meta:
        model = SectMission
        exclude = ['mission']


class MissionSerializer(serializers.ModelSerializer):
    user = serializers.StringRelatedField(read_only=True)
    sections = SectMissionSerializer(many=True)
    distance = serializers.ReadOnlyField()
    carbone = serializers.ReadOnlyField()

    class Meta:
        model = Mission
        fields = '__all__'

    def create(self, validated_data):
        sections_data = validated_data.pop('sections')
        mission = Mission.objects.create(**validated_data)
        for section_data in sections_data:
            SectMission.objects.create(mission=mission, **section_data)
        return mission


class DeplacementDTSerializer(serializers.ModelSerializer):
    user = serializers.StringRelatedField(read_only=True)
    distance = serializers.ReadOnlyField()
    carbone = serializers.ReadOnlyField()

    class Meta:
        model = DeplacementDT
        fields = '__all__'
