from django.db import models
from django.conf import settings
from django.db.models import Sum
from carbon_indi.carbone import get_mission_emission, get_deplacementdt_emission


class Mission(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE,
                             related_name='missions')
    nomVoyage = models.CharField(max_length=50)
    dateDepart = models.DateTimeField()
    dateRetour = models.DateTimeField(null=True)
    isAllerRetour = models.BooleanField()
    dateCreation = models.DateTimeField(auto_now_add=True)

    @property
    def annee(self):
        return self.dateDepart.year

    @property
    def distance(self):
        distance = self.sections.aggregate(sum=Sum('distance'))['sum'] if self.sections.exists() else 0
        return distance * (int(self.isAllerRetour) + 1)

    @property
    def carbone(self):
        carbone = sum([section.carbone for section in self.sections.all()])
        return carbone * (int(self.isAllerRetour) + 1)


class SectMission(models.Model):
    mission = models.ForeignKey(Mission,
                                on_delete=models.CASCADE,
                                related_name='sections')
    modeDeplacement = models.CharField(max_length=250)
    villeDepart = models.CharField(max_length=50)
    paysDepart = models.CharField(max_length=5)
    villeDestination = models.CharField(max_length=50)
    paysDestination = models.CharField(max_length=5)

    nbPersonneVoiture = models.IntegerField(blank=True, null=True)
    distance = models.FloatField(null=True)

    @property
    def carbone(self):
        return get_mission_emission(self)


class DeplacementDT(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE,
                             related_name='deplacementsdt')
    annee = models.IntegerField()
    nomVoyage = models.CharField(max_length=50)
    isAllerRetour = models.BooleanField()
    nbJoursTravail = models.IntegerField()
    marche = models.IntegerField(null=True)
    velo = models.IntegerField(null=True)
    veloElectrique = models.IntegerField(null=True)
    trottinetteElectrique = models.IntegerField(null=True)
    deuxRouesMotorise = models.IntegerField(null=True)
    voiture = models.IntegerField(null=True)
    bus = models.IntegerField(null=True)
    tramway = models.IntegerField(null=True)
    train = models.IntegerField(null=True)
    rer = models.IntegerField(null=True)
    metro = models.IntegerField(null=True)
    nbPersonnes2roues = models.IntegerField(null=True)
    nbPersonnesVoiture = models.IntegerField(null=True)
    motorisation = models.CharField(max_length=25, null=True)
    tailleAgglomeration = models.SmallIntegerField(null=True)

    @property
    def distance(self):
        modesDeplacement = ['marche', 'velo', 'veloElectrique', 'trottinetteElectrique', 'deuxRouesMotorise',
                         'voiture', 'bus', 'tramway', 'train', 'rer', 'metro']
        distance = 0
        for modeDeplacement in modesDeplacement:
            value = getattr(self, modeDeplacement)
            if value:
                distance += value
        return distance

    @property
    def carbone(self):
        return get_deplacementdt_emission(self)
