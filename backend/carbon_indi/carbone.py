import numpy as np
from django.conf import settings

FACTORS = settings.EMISSION_FACTORS

MODES_DEPLACEMENTS = {
  'Avion': 1.0,
  'Train': 1.2,
  'Voiture': 1.3,
  'Taxi': 1.3,
  'Bus': 1.5,
  'Tramway': 1.5,
  'RER': 1.2,
  'Métro': 1.7,
  'Ferry': 1.0
}


def get_facteur_mission(modeDeplacement, distance, missionType='NA'):
    factor = None
    if modeDeplacement == "Avion":
        factor = list(filter(lambda obj: obj['souscategorie'] == 'Avion', FACTORS))
        if distance < 1000:
            factor = list(filter(lambda obj: obj['energietype'] == '<1000 km', factor))[0]
        elif distance < 3501:
            factor = list(filter(lambda obj: obj['energietype'] == '1001-3500km', factor))[0]
        else:
            factor = list(filter(lambda obj: obj['energietype'] == '>3500 km', factor))[0]

    elif modeDeplacement == "Voiture" or modeDeplacement == "Taxi":
        factor = list(filter(lambda obj: obj['souscategorie'] == 'Voiture', FACTORS))
        factor = list(filter(lambda obj: obj['energietype'] == 'Motorisation inconnue', factor))[0]
    elif modeDeplacement == "Bus":
        factor = list(filter(lambda obj: obj['nomBaseCarbone'] == 'Autobus moyen - '
                                                                  'Agglomération de plus de 250 000 habitants',
                             FACTORS))[0]
    elif modeDeplacement == "Train":
        if missionType == 'NA':
            if distance < 200:
                factor = list(filter(lambda obj: obj['energietype'] == 'Train <200 km', FACTORS))[0]
            else:
                factor = list(filter(lambda obj: obj['energietype'] == 'TGV > 200 km', FACTORS))[0]
        elif missionType == "MX":
            factor = list(filter(lambda obj: obj['energietype'] == 'Train mixte France et international', FACTORS))[0]
        else:
            factor = list(filter(lambda obj: obj['energietype'] == 'Train international', FACTORS))[0]
    elif modeDeplacement == "Tramway":
        factor = list(filter(lambda obj: obj['energietype'] == 'Tramway > 250 000 hab', FACTORS))[0]
    elif modeDeplacement == "RER":
        factor = list(filter(lambda obj: obj['energietype'] == 'RER', FACTORS))[0]
    elif modeDeplacement == "Métro":
        factor = list(filter(lambda obj: obj['energietype'] == 'Métro', FACTORS))[0]
    elif modeDeplacement == "Ferry":
        factor = list(filter(lambda obj: obj['energietype'] == 'Ferry', FACTORS))[0]

    factor_tot = factor['decomposition'][0]['total']
    # if modeDeplacement == "Avion" and settings.CONFIG['emissions']['traineesAConsiderer']:
    #     factor_combu = list(filter(lambda obj: obj['nom'] == 'Combustion', factor['decomposition']))[0]
    #     factor_tot = factor['decomposition'][0]['total'] + factor_combu['total']

    return factor_tot


def get_facteur_deplacementdt(modeDeplacement, distance, motorisation, tailleAgglomeration):
    factor = None
    if modeDeplacement == "marche":
        return 0
    elif modeDeplacement == "velo":
        factor = list(filter(lambda obj: obj['souscategorie'] == 'Vélo', FACTORS))
        factor = list(filter(lambda obj: obj['energietype'] == 'Musculaire', factor))[0]
    elif modeDeplacement == "veloElectrique":
        factor = list(filter(lambda obj: obj['souscategorie'] == 'Vélo', FACTORS))
        factor = list(filter(lambda obj: obj['energietype'] == 'Electrique', factor))[0]
    elif modeDeplacement == "trottinetteElectrique":
        factor = list(filter(lambda obj: obj['souscategorie'] == 'Trottinette', FACTORS))[0]
    elif modeDeplacement == "deuxRouesMotorise":
        factor = list(filter(lambda obj: obj['souscategorie'] == 'Moto', FACTORS))[0]
    elif modeDeplacement == "voiture":
        factor = list(filter(lambda obj: obj['souscategorie'] == 'Voiture', FACTORS))
        factor = list(filter(lambda obj: obj['energietype'] == motorisation, factor))
        factor = list(filter(lambda obj: obj['unite'] == 'km', factor))[0]
    elif modeDeplacement == "bus":
        if tailleAgglomeration == 0:
            nom_base = 'Autobus moyen - Agglomération moins de 100 000 habitants'
        elif tailleAgglomeration == 1:
            nom_base = 'Autobus moyen - Agglomération de 100 000 à 250 000 habitants'
        else:
            nom_base = 'Autobus moyen - Agglomération de plus de 250 000 habitants'
        factor = list(filter(lambda obj: obj['nomBaseCarbone'] == nom_base, FACTORS))[0]
    elif modeDeplacement == "train":
        if distance < 200:
            factor = list(filter(lambda obj: obj['energietype'] == 'Train <200 km', FACTORS))[0]
        else:
            factor = list(filter(lambda obj: obj['energietype'] == 'TGV > 200 km', FACTORS))[0]
    elif modeDeplacement == "tramway":
        if tailleAgglomeration == 0:
            energie_type = 'Tramway =< 250 000 hab'
        elif tailleAgglomeration == 1:
            energie_type = 'Tramway =< 250 000 hab'
        else:
            energie_type = 'Tramway > 250 000 hab'
        factor = list(filter(lambda obj: obj['energietype'] == energie_type, FACTORS))[0]
    elif modeDeplacement == "rer":
        factor = list(filter(lambda obj: obj['energietype'] == 'RER', FACTORS))[0]
    elif modeDeplacement == "metro":
        factor = list(filter(lambda obj: obj['energietype'] == 'Métro', FACTORS))[0]

    factor_tot = factor['decomposition'][0]['total']

    return factor_tot


def get_mission_emission(section):
    distance = section.distance
    modeDeplacement = section.modeDeplacement
    nbPersonneVoiture = section.nbPersonneVoiture
    paysDepart = section.paysDepart
    paysDestination = section.paysDestination

    missionType = 'IN'
    if paysDepart == 'FR' and paysDestination == 'FR':
        missionType = 'NA'
    elif paysDepart == 'FR' or paysDestination == 'FR':
        missionType = "MX"

    factor = get_facteur_mission(modeDeplacement, distance, missionType=missionType)
    cdistance = distance
    if modeDeplacement == "Avion":
        cdistance += 95
    elif modeDeplacement == "Taxi":
        cdistance *= (1 + 1 / nbPersonneVoiture)
    elif modeDeplacement == "Voiture":
        cdistance /= nbPersonneVoiture

    cdistance *= MODES_DEPLACEMENTS[modeDeplacement]
    carbone = factor * cdistance

    return carbone


def get_deplacementdt_mode_emission(deplacement, modeDeplacement):
    nbJoursTravail = deplacement.nbJoursTravail
    motorisation = deplacement.motorisation
    tailleAgglomeration = deplacement.tailleAgglomeration
    nbPersonnesVoiture = deplacement.nbPersonnesVoiture
    nbPersonnes2roues = deplacement.nbPersonnes2roues

    carbone = 0
    distance = getattr(deplacement, modeDeplacement)
    if distance:
        factor = get_facteur_deplacementdt(modeDeplacement, distance, motorisation, tailleAgglomeration)
        if modeDeplacement == "voiture":
            distance /= nbPersonnesVoiture
        elif modeDeplacement == "deuxRouesMotorise":
            distance /= nbPersonnes2roues
        carbone += factor * distance

    carbone *= nbJoursTravail
    return carbone


def get_deplacementdt_emission(deplacement):
    modesDeplacement = ['marche', 'velo', 'veloElectrique', 'trottinetteElectrique', 'deuxRouesMotorise',
                        'voiture', 'bus', 'tramway', 'train', 'rer', 'metro']

    carbone = 0
    for modeDeplacement in modesDeplacement:
        carbone += get_deplacementdt_mode_emission(deplacement, modeDeplacement)
    return carbone
