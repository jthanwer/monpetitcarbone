from django.contrib import admin
from .models import SectMission, Mission, DeplacementDT


@admin.register(SectMission)
class SectMissionAdmin(admin.ModelAdmin):
    list_display = ('id', 'mission', 'modeDeplacement', 'villeDepart', 'paysDepart',
                    'villeDestination', 'paysDestination', 'distance')
    list_filter = ('modeDeplacement',)
    ordering = ('-id', 'distance')
    search_fields = ('id',)


@admin.register(Mission)
class MissionAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'distance', 'carbone')
    search_fields = ('id',)
    ordering = ('-id',)


@admin.register(DeplacementDT)
class DeplacementDTAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'distance', 'carbone')
    search_fields = ('id',)
    ordering = ('-id',)
