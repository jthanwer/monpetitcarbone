from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import action

from django.conf import settings
from django.utils.dateparse import parse_datetime
from django.db.models import Prefetch

from carbon_indi.serializers import SectMissionSerializer, MissionSerializer, \
    DeplacementDTSerializer
from carbon_indi.models import SectMission, Mission, DeplacementDT
from carbon_indi.carbone import *


# -- Mission
class MissionViewSet(viewsets.ModelViewSet):
    queryset = Mission.objects.all()
    serializer_class = MissionSerializer
    permission_classes = [IsAuthenticated]

    def filter_queryset(self, queryset):
        annee = self.request.query_params.get('annee', None)
        qs = Mission.objects.prefetch_related(Prefetch('sections',
                                                       queryset=SectMission.objects.order_by('id')))
        if annee:
            qs = qs.filter(user=self.request.user, dateDepart__year=int(annee))
        else:
            qs = qs.filter(user=self.request.user)
        return qs.order_by('-dateDepart')

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = request.user
        serializer.save(user=user)
        serializer.data.get('sections').sort(key=lambda x: x['id'])
        return Response(serializer.data, status=status.HTTP_201_CREATED)


# -- Deplacement Domicile-Travail
class DeplacementDTViewSet(viewsets.ModelViewSet):
    queryset = DeplacementDT.objects.all()
    serializer_class = DeplacementDTSerializer
    permission_classes = [IsAuthenticated]

    def filter_queryset(self, queryset):
        annee = self.request.query_params.get('annee', None)
        if annee:
            qs = queryset.filter(user=self.request.user, annee=int(annee))
        else:
            qs = queryset.filter(user=self.request.user)
        return qs.order_by('-annee')

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = request.user
        serializer.save(user=user)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
