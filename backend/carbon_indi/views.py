# coding=utf-8
from rest_framework.decorators import permission_classes
from django.utils.timezone import localdate

from django.db.models import Min, Max

from carbon_indi.models import SectMission, Mission
from carbon_indi.permissions import IsStaffUser

from rest_framework.decorators import api_view, renderer_classes
from rest_framework.response import Response
from rest_framework.renderers import BrowsableAPIRenderer, JSONRenderer
from rest_framework_csv.renderers import CSVRenderer

from users.models import CustomUser

import unidecode


class Labos1p5Renderer(CSVRenderer):
    header = ['# mission', 'Date de départ', 'Ville de départ', 'Pays de départ', 'Ville de destination',
              'Pays de destination', 'Mode de déplacement', 'Nb de personnes dans la voiture',
              'Aller Retour (OUI si identiques, NON si différents)']
    writer_opts = {
        'delimiter': '\t'
    }


class LabRenderer(CSVRenderer):
    header = ['ID Utilisateur', '# mission', 'Date de départ', 'Date de retour',
              'Ville de départ', 'Pays de départ', 'Ville de destination', 'Pays de destination',
              'Mode de déplacement', 'Nb de personnes dans la voiture',
              'Aller Retour (OUI si identiques, NON si différents)',
              'Empreinte carbone (kgCO2e)', 'Distance (km)']
    writer_opts = {
        'delimiter': '\t'
    }


class UserRenderer(CSVRenderer):
    header = ['# mission', 'Nom mission', 'Date de départ', 'Date de retour', 'Ville de départ',
              'Pays de départ', 'Ville de destination',
              'Pays de destination', 'Mode de déplacement',
              'Nb de personnes dans la voiture', 'Aller Retour (OUI si identiques, NON si différents)',
              'Empreinte carbone (kgCO2e)', 'Distance (km)']
    writer_opts = {
        'delimiter': '\t'
    }


@api_view()
@renderer_classes((BrowsableAPIRenderer, JSONRenderer, Labos1p5Renderer,))
@permission_classes([IsStaffUser])
def export_missions_labos1p5(request):
    content = []
    for user in CustomUser.objects.all():
        sections = SectMission.objects.filter(mission__user=user)
        for section in sections:
            data = {
                '# mission': section.mission.id,
                'Date de départ': localdate(section.mission.dateDepart).strftime("%d/%m/%Y"),
                'Ville de départ': section.villeDepart,
                'Pays de départ': section.paysDepart,
                'Ville de destination': section.villeDestination,
                'Pays de destination': section.paysDestination,
                'Mode de déplacement': section.modeDeplacement.lower(),
                'Nb de personnes dans la voiture': section.nbPersonneVoiture,
                'Aller Retour (OUI si identiques, NON si différents)': 'Oui' if section.mission.isAllerRetour else 'Non',
            }
            content.append(data)
    return Response(content)


@api_view()
@renderer_classes((BrowsableAPIRenderer, JSONRenderer, UserRenderer,))
def export_missions_user(request):
    content = []
    user = request.user
    sections = SectMission.objects.filter(mission__user=user)
    for section in sections:
        dateRetour = localdate(section.mission.dateRetour).strftime("%d/%m/%Y") \
            if section.mission.dateRetour else 'Aucun'
        print(type(section.mission.dateDepart))
        data = {
            '# mission': section.mission.id,
            'Nom mission': section.mission.nomVoyage,
            'Date de départ': localdate(section.mission.dateDepart).strftime("%d/%m/%Y"),
            'Date de retour': dateRetour,
            'Ville de départ': section.villeDepart,
            'Pays de départ': section.paysDepart,
            'Ville de destination': section.villeDestination,
            'Pays de destination': section.paysDestination,
            'Mode de déplacement': section.modeDeplacement.lower(),
            'Nb de personnes dans la voiture': section.nbPersonneVoiture,
            'Aller Retour (OUI si identiques, NON si différents)': 'Oui' if section.mission.isAllerRetour else 'Non',
            'Empreinte carbone (kgCO2e)': '{:.1f}'.format(section.carbone * (1 + section.mission.isAllerRetour)),
            'Distance (km)': '{:.1f}'.format(section.distance * (1 + section.mission.isAllerRetour)),
        }
        content.append(data)
    return Response(content)


@api_view()
@renderer_classes((BrowsableAPIRenderer, JSONRenderer, Labos1p5Renderer,))
def export_missions_user_labos1p5(request):
    content = []
    user = request.user
    sections = SectMission.objects.filter(mission__user=user)
    for section in sections:
        data = {
            '# mission': section.mission.id,
            'Date de départ': localdate(section.mission.dateDepart).strftime("%d/%m/%Y"),
            'Ville de départ': section.villeDepart,
            'Pays de départ': section.paysDepart,
            'Ville de destination': section.villeDestination,
            'Pays de destination': section.paysDestination,
            'Mode de déplacement': section.modeDeplacement.lower(),
            'Nb de personnes dans la voiture': section.nbPersonneVoiture,
            'Aller Retour (OUI si identiques, NON si différents)': 'Oui' if section.mission.isAllerRetour else 'Non',
        }
        content.append(data)
    return Response(content)


@api_view()
def get_chart_data(request):
    user = request.user
    qs_missions = user.missions.all()
    qs_deplacementsdt = user.deplacementsdt.all()
    anneesMin = []
    anneesMax = []
    if qs_missions.exists():
        anneesMin.append(qs_missions.aggregate(min_date=Min('dateDepart'))['min_date'].year)
        anneesMax.append(qs_missions.all().aggregate(max_date=Max('dateDepart'))['max_date'].year)
    if qs_deplacementsdt.exists():
        anneesMin.append(qs_deplacementsdt.aggregate(min_date=Min('annee'))['min_date'])
        anneesMax.append(qs_deplacementsdt.all().aggregate(max_date=Max('annee'))['max_date'])

    if len(anneesMin) == 0:
        anneeMin = 2021

    if len(anneesMax) == 0:
        anneeMax = 2021

    else:
        anneeMin = min(anneesMin)
        anneeMax = max(anneesMax)

    annees = list(range(anneeMin, anneeMax + 1))
    data = {'annees': annees, 'total': {}, 'missions': {}, 'domicileTravail': {}}

    # -- Missions
    fields = ['Avion', 'Train', 'Taxi', 'Voiture', 'RER', 'Ferry', 'Métro', 'Tramway', 'Bus']
    for field in fields:
        key = unidecode.unidecode(field.lower())
        data['missions'][key] = [user.missionSubCat(field, annee) / 1000 for annee in annees]
    data['total']['missions'] = [user.mission(annee) / 1000 for annee in annees]

    # -- Déplacements Domicile Travail
    fields = ['marche', 'velo', 'veloElectrique', 'trottinetteElectrique', 'deuxRouesMotorise',
              'voiture', 'bus', 'tramway', 'train', 'rer', 'metro']
    for field in fields:
        data['domicileTravail'][field] = [user.deplacementdtSubCat(field, annee) / 1000 for annee in annees]
    data['total']['domicileTravail'] = [user.deplacementdt(annee) / 1000 for annee in annees]

    return Response(data)
