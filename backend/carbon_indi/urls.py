from django.urls import path, include
from rest_framework.routers import DefaultRouter

from carbon_indi import viewsets as vs, views as v

router = DefaultRouter()
router.register(r'mission', vs.MissionViewSet)
router.register(r'deplacementdt', vs.DeplacementDTViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('chart/data/', v.get_chart_data, name='chart_data'),
]