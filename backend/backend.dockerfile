# --------------
# -- Backend
# --------------
FROM python:3.7 AS backend
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
COPY data /app/data

# -- Continue backend build
WORKDIR /app/backend
RUN pip install --upgrade pip
COPY backend/requirements.txt .
RUN pip install -r requirements.txt 
RUN pip install psycopg2-binary gunicorn
COPY backend ./

# -- Entrypoint
COPY docker-entrypoint.sh /entrypoint.sh
RUN chmod u+x /entrypoint.sh