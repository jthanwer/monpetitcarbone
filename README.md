# Objectif

MonPetitCarbone permet au personnel d'un laboratoire de recherche de suivre l'impact carbone de ses activités au cours des années. Pour l'instant, seule la catégorie Missions (détaillée ci-après) est implémentée mais d'autres suivront. 

MonPetitCarbone s'inscrit dans le cadre de l'impulsion initiée par le collectif Labos1point5 qui vise à tendre vers une recherche moins carbonée, plus en accord avec les enjeux actuels. Cette réduction passe par la création de tels outils permettant aux personnels de se renseigner et de s'interroger sur l'impact généré par leurs activités sur le climat. 

Cet outil a été codé sous la forme d'une application web en VueJS/Buefy pour le frontend et Python/Django/Django REST Framework pour le backend. L'application peut-être installée sur des serveurs dédiés, par exemple sur un intranet. 

# Catégories d'émissions

Les activités de l'outil sont classées en catégories d'émissions :

* Missions (implémentée)
* Déplacements domicile-travail (très bientôt implémentée)
* Numérique (bientôt implémentée)
* Achats (projet d'être implémentée)

La méthodologie de l'empreinte carbone utilisée suit celle de Labos1point5 et s'appuie fortement sur les facteurs d'émissions suggérés par l'ADEME. Les géodésiques sont utilisées dans le calcul des distances.

## Missions

Cette catégorie est implémentée. Elle représente l'ensemble des déplacements effectués dans le cadre de missions. Ces déplacements peuvent être effectués en utilisant de nombreux modes de déplacements. Les villes de départ et d'arrivée sont choisies en utilisant la base de données Geonames. 

## Déplacements domicile-travail
Cette catégorie n'est pas encore implémentée. Elle correspond aux déplacements réguliers entre le domicile et le travail. Ce déplacement peut être fait à l'aide d'une voiture personnelle, de transports en communs (bus, RER), d'un vélo (électrique ou basique) ou encore d'une trotinette électrique.
## Numérique
Cette catégorie n'est pas encore implémentée. Certains chercheurs utilisent des supercalculateurs ou des clusters locaux dans le cadre de leur recherche. Chaque heure de calcul engendre une utilisation significative d'électricité (et donc des émissions directes et indirectes).

## Achats
Cette catégorie n'est pas encore implémentée. Elle correspond aux achats réalisés (ordinateur, matériel de bureau, papier, etc...)
